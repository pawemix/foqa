module Main where

import Data.Either
import Foqa
import Parser (ParserError (CouldNotRecognize, EmptyInput))
import System.IO
import TargetTree

main = do
  input <- getContents
  case parse input of
    (Left EmptyInput) ->
      hPutStrLn stderr "ERROR: no input was given"
    (Left (CouldNotRecognize str)) ->
      hPutStr stderr $ "ERROR: could not parse\n" ++ str
    (Right progs) -> case foldl1 (<>) (compile <$> progs) of
      (Left (FuncNotDef name)) ->
        hPutStrLn stderr $ "ERROR: function `" <> name <> "` not defined"
      (Left (TooLittleArguments fn)) ->
        hPutStrLn stderr $ "ERROR: not enough arguments supplied for " <> fn
      (Left (TooManyArguments fn args)) ->
        hPutStrLn stderr $
          "ERROR: too many arguments supplied for " <> fn <> ": " <> show args
      (Right shExpr) -> case render shExpr of
        (Left (IllegalOperatorName name)) ->
          hPutStrLn stderr $ "ERROR: illegal operator: " <> name
        (Right output) -> putStrLn output

-- TODO: use Foqa.foqa :: String -> Either FoqaError ShExpr
-- Foqa.foqa shall include logic of piping `parse` with `compile`
