# To Do

## Minimum for "released" 0.1.0

* [X] parser:
    * [X] literals
    * [X] function definitions
    * [X] function calls
    * [X] operator definitions
    * [X] operator calls
* [X] `eval` builtin primitive
    * [X] output shall be treated as shell code verbatim
    * [X] variable amount of arguments
    * [X] concats them as-is with no delimiter
* [X] functions/operators - accept arguments and bind them to result expressions
* [X] compile operator definitions
* [X] nested function/operator calls (with parentheses and more)
* [X] translate operator names to valid POSIX-sh function names
    * _In the shell command language, a word consisting solely of underscores, digits, and alphabetics from the portable character set. The first character of a name is not a digit._
    * <https://www.ascii-code.com/>
* [X] check for function names colliding with POSIX-sh keywords
* [X] allow recursion
* [X] check for a proper amount of arguments passed to a function
* [ ] finished pattern matching for one parameter, for strings and numbers
* [ ] laziness / simple short-circuiting (by inlining functions, or?)
    * evaluate only in two cases:
        * case function - it needs its condition evaluated
        * main entry point
    * constants are `printf` anonymous functions
* [ ] some indentation required for multiline statements
* [ ] simple commandline interface (e.g. read and concatenate multiple .fq files)

## For 0.2.0

* [ ] multi-parameter pattern-matching
* [ ] typechecking & type definitions
    * primitives like String (Word? Line?), Number
    * records implemented either as **escaped TSV** or newline-separated fields
    * functions in records - either auto-generated function name references or function literals to be `eval`ed
* [ ] pattern matching on new typed primitives / records
* [ ] operator associativity & precedence

## In General

* [ ] lambda expressions
* [ ] inline (some) functions
* [ ] include standard library (`std.fq`?) by default
    * common operations like `+`, `++`, `-` etc. should be here
* [ ] module definition & module importing
    * URL? <https://dhall-lang.org>
* [ ] recognize `main` as executable's entry point
* [ ] libraries without `main` shall be allowed
* [ ] disallow expressions (not definitions) in file's root contents
    * as it doesn't make sense from Haskell's point of view
* [ ] parser/compiler/rendere errors - inform about error's location
* [ ] recursion - tail call optimization
* [ ] recursion - warn if infinite loop risk

