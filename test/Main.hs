{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}

module Main (main) where

import Data.List.NonEmpty
import Foqa
import Parser (ParserError (EmptyInput))
import SourceTree
import TargetTree
import TargetTreeTest
import Test.HUnit

main :: IO ()
main =
  runTestTTAndExit $
    TestList [testParse, testCompile, testReplaceIllegalSymbols, testRender]

getVar = ShGetVar

raw = ShRaw

aProgram = pure . pure . fromList

parseAndCompile = fmap (fmap compile) . parse

infixr 6 %$

class DslLiteral output where
  str :: String -> output
  num :: Int -> output

instance DslLiteral Expression where
  str = LitExpr . Str
  num = LitExpr . Num

instance DslLiteral Param where
  str = Lit . Str
  num = Lit . Num

instance DslLiteral ShArg where
  str = ShLit
  num = ShLit . show

class DslCall params output | output -> params where
  (%$) :: String -> params -> output
  call :: String -> output

instance DslCall [ShArg] ShExpr where
  cmdName %$ args = ShRun $ ShLit cmdName :| args
  call cmd = cmd %$ mempty

instance DslCall [Expression] Expression where
  fnName %$ expr = FuncCall fnName expr
  call cmd = cmd %$ mempty

instance DslCall [Param] (String, [Param]) where
  (%$) = (,)
  call f = f %$ mempty

infixr 5 %=

class DslDef header body output | output -> header body where
  (%=) :: header -> body -> output

instance DslDef String ShExpr ShExpr where
  name %= body = ShFuncDef name body

instance DslDef (String, [Param]) Expression Definition where
  (fnName, params) %= fnBody = FuncDef fnName params fnBody

testParse :: Test
testParse =
  "test foqa-parse"
    ~: [ "empty input"
           ~: Left EmptyInput ~=? parse "",
         "number literal"
           ~: aProgram [Exp $ num 69] ~=? parse "69",
         "string literal"
           ~: aProgram [Exp $ str "hello"] ~=? parse "\"hello\"",
         "string literal with escapes"
           ~: aProgram [Exp $ str "hello\nthere"]
           ~=? parse "\"hello\\nthere\"",
         "some whitespace after"
           ~: aProgram [Exp $ num 42]
           ~=? parse "42\n     \n",
         "some whitespace before"
           ~: aProgram [Exp $ num 42]
           ~=? parse "\n    \t\n42",
         "function call"
           ~: aProgram [Exp $ "foo" %$ [num 42]]
           ~=? parse "foo 42",
         "function call, two arguments"
           ~: aProgram [Exp $ "foo" %$ [num 42, str "bar"]]
           ~=? parse "foo 42 \"bar\"",
         "answer = 42"
           ~: aProgram [Def $ call "answer" %= num 42]
           ~=? parse "answer = 42",
         "answer = \"Who knows?\""
           ~: aProgram [Def $ call "answer" %= str "Who knows?"]
           ~=? parse "answer = \"Who knows?\"",
         "aliasing constant"
           ~: aProgram [Def $ call "answer" %= call "whoKnows"]
           ~=? parse "answer = whoKnows",
         "binary operator const definition"
           ~: aProgram [Def $ OperDef "<->" (Id "a") (Id "b") (num 42)]
           ~=? parse "a <-> b = 42",
         "a + 2"
           ~: aProgram [Exp $ OperCall "+" (call "a") (num 2)]
           ~=? parse "a + 2",
         "inc 15 + 2"
           ~: aProgram [Exp $ OperCall "+" ("inc" %$ [num 15]) (num 2)]
           ~=? parse "inc 15 + 2",
         "2 + 2 * 2"
           ~: aProgram
             [Exp $ OperCall "+" (num 2) (OperCall "*" (num 2) (num 2))]
           ~=? parse "2 + 2 * 2",
         "2 + (2 * 2)"
           ~: aProgram
             [Exp $ OperCall "+" (num 2) (OperCall "*" (num 2) (num 2))]
           ~=? parse "2 + (2 * 2)",
         "multiple phrases"
           ~: aProgram
             [ Def $ "inc" %$ [Id "n"] %= OperCall "+" (call "n") (num 1),
               Def $ call "main" %= "echo" %$ ["inc" %$ [num 15]]
             ]
           ~=? parse "inc n = n + 1; main = echo (inc 15)",
         "multiple lines"
           ~: aProgram
             [ Def $ "inc" %$ [Id "n"] %= OperCall "+" (call "n") (num 1),
               Def $ call "main" %= "echo" %$ ["inc" %$ [num 15]]
             ]
           ~=? parse "inc n = n + 1\nmain = echo (inc 15)",
         "three parameters, newline-separated" -- FIXME: make it more idiomatic
           ~: let expected :: Program
                  expected =
                    fromList
                      [ Def $ "snd" %$ [Id "x", Id "y", Id "z"] %= call "y",
                        Exp $ "snd" %$ [num 15, num 32, num 64]
                      ]
                  find x xs = if x `elem` xs then pure x else xs
               in (pure . pure) expected
                    ~=? find expected <$> parse "snd x y z = y\nsnd 15 32 64",
         "pattern match on one param with param hiding"
           ~: aProgram
             [ Def $ "if" %$ [str "", Id "_", Id "snd"] %= call "snd",
               Def $ "if" %$ [Id "_", Id "fst", Id "_"] %= call "fst"
             ]
           ~=? parse "if \"\" _ snd = snd; if _ fst _ = fst"
           -- TODO: what if "_" gets used as argument?
       ]

testCompile :: Test
testCompile =
  "test compile"
    ~: [ "just eval call, no parameters"
           ~: (pure . pure . pure) (raw $ str "echo \"test\"")
           ~=? parseAndCompile "eval \"echo \\\"test\\\"\"",
         "simple func def + its call"
           ~: (pure . mconcat)
             [ "answer" %= "printf" %$ [str "%d\\n", num 42],
               call "answer"
             ]
           ~=? (compile . fromList)
             [ Def $ call "answer" %= num 42,
               Exp $ call "answer"
             ],
         "`id` function call and definition"
           ~: (pure . mconcat)
             [ "id" %= "printf" %$ [str "%s\\n", getVar "1"],
               -- TODO: printf %d\\n ?  ^^^^^^^
               "id" %$ [num 15]
             ]
           ~=? (compile . fromList)
             [ Def $ "id" %$ [Id "n"] %= call "n",
               Exp $ "id" %$ [num 15]
             ],
         "func call - multiple arguments: `snd x y = y; snd 32 64`"
           ~: (pure . mconcat)
             [ "snd" %= "printf" %$ [str "%s\\n", getVar "2"],
               "snd" %$ [num 32, num 64]
             ]
           ~=? (compile . fromList)
             [ Def $ "snd" %$ [Id "x", Id "y"] %= call "y",
               Exp $ "snd" %$ [num 32, num 64]
             ],
         "eval call - multiple parameters; eval uses parameters"
           ~: (pure . pure . pure)
             ( "add"
                 %= (raw . mconcat)
                   [ str "echo $((",
                     getVar "1",
                     str " + ",
                     getVar "2",
                     str "))"
                   ]
             )
           ~=? fmap compile
             <$> parse "add a b = eval \"echo $((\" a \" + \" b \"))\"",
         "legal operator definition"
           ~: (pure . pure . pure)
             ( "+"
                 %= (raw . mconcat)
                   [ str "echo $((",
                     getVar "1",
                     str " + ",
                     getVar "2",
                     str "))"
                   ]
             )
           ~=? parseAndCompile "a + b = eval \"echo $((\" a \" + \" b \"))\"",
         "nested function call"
           ~: (pure . pure . pure . mconcat)
             [ "+" %= "printf" %$ [str "%d\\n", num 42],
               "inc" %= "+" %$ [getVar "1", num 1],
               "+" %$ [num 15, ShSubsh $ "inc" %$ [num 14]]
             ]
           ~=? parseAndCompile "a + b = 42\ninc n = n + 1\n15 + (inc 14)",
         "nested operator call"
           ~: (pure . pure . pure . mconcat)
             [ "+" %= "printf" %$ [str "%d\\n", num 42],
               "+" %$ [num 2, ShSubsh $ "+" %$ [num 3, num 4]]
             ]
           ~=? fmap compile <$> parse "a + b = 42; 2 + 3 + 4",
         "nested eval-using function call"
           ~: (pure . pure . pure . mconcat)
             [ "+" %= ShRaw (str "echo $((" <> getVar "1" <> str " + " <> getVar "2" <> str "))"),
               "+" %$ [num 2, ShSubsh $ "+" %$ [num 3, num 4]]
             ]
           ~=? parseAndCompile
             "a + b = eval \"echo $((\" a \" + \" b \"))\"; 2 + 3 + 4",
         "recursive operator def"
           ~: (pure . pure . pure)
             ("+" %= "+" %$ [getVar "2", getVar "1"])
           ~=? parseAndCompile "a + b = b + a",
         "too many arguments given to a parameter"
           ~: (pure . pure . Left . TooManyArguments "n" . fromList)
             [num 1, num 2]
           ~=? parseAndCompile "id n = (n 1 2)",
         "too many arguments given to a function"
           ~: (pure . pure . Left . TooManyArguments "id" . fromList)
             [call "n", call "n"]
           ~=? parseAndCompile "id n = id n n",
         "too little arguments given to a function"
           ~: (pure . pure . Left . TooLittleArguments) "trd"
           ~=? parseAndCompile "trd x y z = z; sth n = trd n n",
         "nonexistent function call"
           ~: (pure . pure . Left . FuncNotDef) "somefunc"
           ~=? parseAndCompile "somefunc 16 32 64",
         "inverted order of definitions"
           ~: (pure . pure . pure . mconcat)
             [ "snd" %= "printf" %$ [str "%s\\n", getVar "2"],
               "snd" %$ [num 15, num 32]
             ]
           ~=? parseAndCompile "snd 15 32; snd x y = y",
         "multiple parameter hiding"
           ~: (pure . mconcat)
             [ "trd" %= ("printf" %$ [str "%s\\n", getVar "3"]),
               "trd" %$ [num 15, num 42, num 58]
             ]
           ~=? (compile . fromList)
             [ Def $ "trd" %$ [Id "_", Id "_", Id "x"] %= call "x",
               Exp $ "trd" %$ [num 15, num 42, num 58]
             ],
         "simple pattern match on one string-typed param"
           ~: (pure . mconcat)
             [ "myif"
                 %= ShCase
                   (getVar "1")
                   [ (str "", "printf" %$ [str "%s\\n", getVar "3"]),
                     (str "*", "printf" %$ [str "%s\\n", getVar "2"])
                   ]
             ]
           ~=? (compile . fromList)
             [ Def $ "myif" %$ [str "", Id "_", Id "else"] %= call "else",
               Def $ "myif" %$ [Id "_", Id "then", Id "_"] %= call "then"
             ],
         "expression invoking pattern-matching function"
           ~: (pure . mconcat)
             [ "id" %= "if" %$ [str "", getVar "1", getVar "1"],
               "if"
                 %= ShCase
                   (getVar "1")
                   [ (str "", "printf" %$ [str "%s\\n", getVar "3"]),
                     (str "*", "printf" %$ [str "%s\\n", getVar "2"])
                   ]
             ]
           ~=? (compile . fromList)
             [ Def $ "if" %$ [str "", Id "_", Id "sad"] %= call "sad",
               Def $ "if" %$ [Id "_", Id "happy", Id "_"] %= call "happy",
               Def $ "id" %$ [Id "n"] %= "if" %$ [str "", call "n", call "n"]
             ]
             -- TODO: different blocks apply same function placeholder to
             -- different amount of args
       ]
