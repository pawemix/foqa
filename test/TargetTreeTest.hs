module TargetTreeTest where

import TargetTree
import Test.HUnit

testReplaceIllegalSymbols :: Test
testReplaceIllegalSymbols =
  "replaceIllegalSymbols"
    ~: [ "legal char"
           ~: pure "c" ~=? replaceIllegalSymbols "c",
         "legal string"
           ~: pure "somefunc" ~=? replaceIllegalSymbols "somefunc",
         "illegal char"
           ~: pure "_exc" ~=? replaceIllegalSymbols "!",
         "illegal chars"
           ~: pure "_ltn_pip_gtn" ~=? replaceIllegalSymbols "<|>",
         "reserved string"
           ~: Left "=" ~=? replaceIllegalSymbols "=",
         "mix illegal with llegal"
           ~: Left "_somefunc"
           ~=? replaceIllegalSymbols "_somefunc",
         let allSymbols = "!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
             allSymbolsTranslated = replaceIllegalSymbols . pure <$> allSymbols
             pairAllUp values = (,) <$> values <*> values
             takeDuplicates = filter $ uncurry (==)
          in "there are no duplicate translations"
               ~: length allSymbols
               ~=? (length . takeDuplicates . pairAllUp) allSymbolsTranslated
       ]

testRender :: Test
testRender =
  "test render"
    ~: [ "backslashes escaped if unquoted literal string"
           ~: pure "'hello\\nthere'"
           ~=? render (ShLit "hello\\nthere"),
         "singly quoted literal string with space"
           ~: pure "'hello there'"
           ~=? render (ShLit "hello there")
       ]
