module TargetTree where

import Control.Applicative
import Data.Bifunctor
import Data.Either
import Data.List.NonEmpty

data ShPhrase
  = ShExpr ShExpr
  | ShArg ShArg
  deriving (Eq, Show)

data ShExpr
  = ShAnd ShExpr ShExpr
  | ShFuncDef String ShExpr
  | ShRun (NonEmpty ShArg)
  | ShCase ShArg [(ShArg, ShExpr)]
  | ShRaw ShArg
  | ShNull
  deriving (Eq, Show)

data ShArg
  = ShConcat ShArg ShArg
  | ShLit String
  | ShGetVar String
  | ShSubsh ShExpr
  deriving (Eq, Show)

newtype RendererError
  = IllegalOperatorName String
  deriving (Eq, Show)

class Render t where
  render :: t -> Either RendererError String

(<<>>) :: (Applicative f, Semigroup a) => f a -> f a -> f a
(<<>>) = liftA2 (<>)

instance Render ShPhrase where
  render (ShExpr e) = render e
  render (ShArg a) = render a

instance Render ShExpr where
  render (ShAnd a b) =
    render a <<>> pure " && " <<>> render b
  render (ShFuncDef name contents) =
    cleanedUp name
      <<>> pure "() { "
      <<>> render contents
      <<>> pure "; }"
    where
      cleanedUp =
        first IllegalOperatorName
          . fmap replaceShKeywords
          . replaceIllegalSymbols
  render (ShRun (fstArg :| restArgs)) =
    foldl (\a b -> a <> " " <> b)
      <$> cleanedUp fstArg
      <*> mapM render restArgs
    where
      cleanedUp arg =
        first IllegalOperatorName
          . fmap replaceShKeywords
          . replaceIllegalSymbols
          =<< render arg
  render (ShCase match cases) =
    pure "case " <<>> render match <<>> pure " in "
      <<>> renderCases
      <<>> pure " esac"
    where
      renderCases = mconcat <$> mapM renderCase cases
      renderCase (pattern, expr) =
        pure "(" <<>> render pattern <<>> pure ") "
          <<>> render expr
          <<>> pure ";; "
  render (ShRaw arg) = renderRaw arg
    where
      renderRaw (ShConcat a b) = renderRaw a <<>> renderRaw b
      renderRaw (ShLit str) = pure str
      renderRaw (ShGetVar name) = pure $ "$" <> name
      renderRaw (ShSubsh expr) = pure "$(" <<>> render expr <<>> pure ")"
  render ShNull = pure mempty

instance Render ShArg where
  render (ShConcat a b) = render a <<>> render b
  render (ShLit str)
    | shouldBeEscaped str = pure $ "'" <> singleQuotesEscaped <> "'"
    | otherwise = pure str
    where
      shouldBeEscaped "" = True
      shouldBeEscaped str = any (`elem` "' \t\n\\") str
      singleQuotesEscaped = str >>= escapeSingleQuote
      escapeSingleQuote '\'' = "'\\''"
      escapeSingleQuote c = return c
  render (ShGetVar name) = pure $ "\"$" <> name <> "\""
  render (ShSubsh e) = pure "\"$(" <<>> render e <<>> pure ")\""

instance Semigroup ShExpr where
  ShNull <> e2 = e2
  e1 <> ShNull = e1
  e1 <> e2 = ShAnd e1 e2

instance Monoid ShExpr where
  mempty = ShNull

instance Semigroup ShArg where
  ShLit "" <> a = a
  a <> ShLit "" = a
  a1 <> a2 = ShConcat a1 a2

instance Monoid ShArg where
  mempty = ShLit ""

replaceIllegalSymbols :: String -> Either String String
replaceIllegalSymbols str = do
  reservedFilteredStr <- filterReserved str
  let symbolsReplaced = replaceIllegalSymbol <$> str
  case partitionEithers symbolsReplaced of
    (legals, []) -> return legals
    ([], illegals) -> return (mconcat illegals)
    _ -> Left str
  where
    filterReserved str = case str of
      "=" -> Left "="
      _ -> pure str
    replaceIllegalSymbol :: Char -> Either Char String
    replaceIllegalSymbol c = case c of
      '!' -> pure "_exc"
      '"' -> pure "_dqt"
      '#' -> pure "_hsh"
      '$' -> pure "_dlr"
      '%' -> pure "_prc"
      '&' -> pure "_amp"
      '\'' -> pure "_sqt"
      '(' -> pure "_opr"
      ')' -> pure "_cpr"
      '*' -> pure "_ast"
      '+' -> pure "_pls"
      ',' -> pure "_com"
      '-' -> pure "_mns"
      '.' -> pure "_dot"
      '/' -> pure "_fsl"
      ':' -> pure "_col"
      ';' -> pure "_scl"
      '<' -> pure "_ltn"
      '=' -> pure "_eql"
      '>' -> pure "_gtn"
      '?' -> pure "_qst"
      '@' -> pure "_ats"
      '[' -> pure "_osb"
      '\\' -> pure "_bsl"
      ']' -> pure "_csb"
      '^' -> pure "_crt"
      '_' -> pure "_und"
      '`' -> pure "_btc"
      '{' -> pure "_obr"
      '|' -> pure "_pip"
      '}' -> pure "_cbr"
      '~' -> pure "_tld"
      _ -> Left c

replaceShKeywords :: String -> String
replaceShKeywords str =
  if str `elem` shKeywords then '_' : str else str
  where
    shKeywords =
      [ "case",
        "do",
        "done",
        "elif",
        "else",
        "esac",
        "fi",
        "for",
        "if",
        "in",
        "then",
        "until",
        "while"
      ]
