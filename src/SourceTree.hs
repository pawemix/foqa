module SourceTree where

import Control.Applicative
import Data.Char
import Data.Functor.Compose
import Data.List.NonEmpty
import Parser
import System.IO

-- TODO: Program -> ProgramFile?
-- TODO: Script [Phrase] | Module String [Phrase]
type Program =
  NonEmpty Phrase

data Phrase
  = Def Definition
  | Exp Expression
  deriving (Eq)

instance Show Phrase where
  show (Def d) = show d
  show (Exp e) = show e

data Definition
  = FuncDef String [Param] Expression -- TODO: FuncAnnot (Type)
  | OperDef String Param Param Expression
  deriving (Eq)

instance Show Definition where
  show (FuncDef name [] expr) =
    show name <> " %= " <> show expr
  show (FuncDef name params expr) =
    show (name, params) <> " %= " <> show expr
  show (OperDef name a b expr) =
    show a <> " %> " <> show name <> " %< " <> show b <> " %= " <> show expr

data Expression
  = LitExpr Literal
  | FuncCall String [Expression]
  | OperCall String Expression Expression
  deriving (Eq)

instance Show Expression where
  show (LitExpr lit) = show lit
  show (FuncCall name []) = show name
  show (FuncCall name exprs) = show name <> " %$ " <> show exprs
  show (OperCall name e1 e2) =
    show e1 <> " %> " <> show name <> " %< " <> show e2

data Param
  = Id String
  | Lit Literal
  deriving (Eq, Show, Ord)

data Literal
  = Str String
  | Num Int
  deriving (Eq, Ord)

instance Show Literal where
  show (Str str) = show str
  show (Num num) = show num

oblGapP = someCondP isSpace

optGapP = manyCondP isSpace

oblPadP = someCondP (`elem` " \t")

optPadP = manyCondP (`elem` " \t")

identifierP :: Parser String
identifierP = someCondP $ \c -> isAlpha c || c == '_'

operatorP :: Parser String
operatorP = some $ condP isRestrictedSymbol
  where
    isRestrictedSymbol c =
      (isSymbol c || isPunctuation c)
        && c `notElem` "='\";_"

-- TODO: revise what chars to allow
-- NOTE: sh allows dashes and underscores and purely-number-strings
-- NOTE: haskell will treat purely-number-strings as number literals

paramP :: Parser Param
paramP = (Id <$> identifierP) <|> (Lit <$> literalP)

exprP :: Parser Expression
exprP = operCallP <|> funcCallP <|> (LitExpr <$> literalP) <|> parenExprP

parenExprP :: Parser Expression
parenExprP = charP '(' *> optGapP *> exprP <* optGapP <* charP ')'

constP :: Parser Expression
constP = flip FuncCall empty <$> identifierP

funcCallP :: Parser Expression
funcCallP = FuncCall <$> identifierP <*> many (oblGapP *> argP)
  where
    argP =
      (LitExpr <$> literalP)
        <|> constP
        <|> parenExprP

operCallP :: Parser Expression
operCallP =
  flip OperCall
    <$> (fmap LitExpr literalP <|> funcCallP <|> parenExprP)
    <*> (optGapP *> operatorP <* optGapP)
    <*> exprP

literalP :: Parser Literal
literalP =
  (Str <$> (quoteP *> inQuotesP <* quoteP))
    <|> (Num . read <$> someCondP isNumber) -- FIXME: readMaybe
  where
    inQuotesP = greedy $ many $ escCharP <|> condP (/= '"')
    quoteP = charP '"'

escCharP :: Parser Char
escCharP =
  ('\n' <$ strP "\\n")
    <|> ('\r' <$ strP "\\r")
    <|> ('\t' <$ strP "\\t")
    <|> ('\\' <$ strP "\\\\")
    <|> ('"' <$ strP "\\\"")

operDefP :: Parser Definition
operDefP =
  flip OperDef
    <$> paramP
    <*> (optGapP *> operatorP <* optGapP)
    <*> paramP
    <*> (optGapP *> charP '=' *> optGapP *> exprP)

funcDefP :: Parser Definition
funcDefP =
  FuncDef
    <$> identifierP
    <*> many (oblGapP *> paramP)
    <* optGapP
    <* charP '='
    <* optGapP
    <*> exprP

defP :: Parser Definition
defP = operDefP <|> funcDefP

progP :: Parser Program
progP =
  (:|) <$> (optGapP *> phraseP) <*> many (phraseSepP *> phraseP) <* optGapP
  where
    phraseSepP = optPadP *> (charP '\n' <|> charP ';') <* optGapP
    phraseP = (Def <$> defP) <|> (Exp <$> exprP)
