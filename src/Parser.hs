module Parser where

import Control.Applicative
import Data.Char
import Data.Foldable
import Data.Semigroup

data ParserError
  = CouldNotRecognize String
  | EmptyInput
  deriving (Eq, Show)

newtype Parser o = Parser {parse :: String -> [(String, o)]}

instance Functor Parser where
  fmap f (Parser p) = Parser $ fmap (fmap f) . p

instance Applicative Parser where
  pure x = Parser $ \str -> pure (str, x)
  p1 <*> p2 = Parser $ \s -> do
    (s', f) <- p1 `parse` s
    (s'', x) <- p2 `parse` s'
    return (s'', f x)

instance Alternative Parser where
  empty = Parser $ const empty
  p1 <|> p2 = Parser $ \s -> p1 `parse` s <|> p2 `parse` s

instance Monad Parser where
  (Parser pa) >>= fpb = Parser $ \s -> do
    (rest, a) <- pa s
    fpb a `parse` rest

instance Semigroup o => Semigroup (Parser o) where
  p1 <> p2 = (<>) <$> p1 <*> p2

instance Monoid o => Monoid (Parser o) where
  mempty = Parser $ \s -> pure (s, mempty)

condP :: (Char -> Bool) -> Parser Char
condP cond = Parser parse
  where
    parse "" = empty
    parse (c : str)
      | cond c = pure (str, c)
      | otherwise = empty

charP :: Char -> Parser Char
charP char = condP (== char)

strP :: String -> Parser String
strP string = sequenceA $ charP <$> string

greedy :: Parser a -> Parser a
greedy (Parser p) = Parser $ best . p
  where
    best :: [(String, a)] -> [(String, a)]
    best [] = empty
    best xs = pure . minimumBy lenOfLeftovers $ xs
    lenOfLeftovers (left1, _) (left2, _) = length left1 `compare` length left2

someCondP :: (Char -> Bool) -> Parser String
someCondP cond = Parser $ matchNonempty . span cond
  where
    matchNonempty :: (String, String) -> [(String, String)]
    matchNonempty ("", rest) = empty
    matchNonempty (match, rest) = pure (rest, match)

manyCondP :: (Char -> Bool) -> Parser String
manyCondP cond = Parser $ pure . flip . span cond
  where
    flip (fst, snd) = (snd, fst)
