module Foqa where

import Control.Applicative
import Control.Monad.Except (ExceptT (ExceptT), runExcept)
import Control.Monad.State
import Control.Monad.Writer (execWriter)
import Data.Bifunctor
import Data.Either
import Data.List.NonEmpty hiding (insert, length, singleton)
import Data.Map hiding (empty, foldl)
import Data.Maybe
import Parser
import SourceTree
import TargetTree

parse :: String -> Either ParserError (NonEmpty Program)
parse = giveSolutionElseErrors . fmap findPossible . Parser.parse progP
  where
    findPossible ("", prog) = pure prog
    findPossible (str, _) = Left str
    giveSolutionElseErrors vals =
      case nonEmpty $ rights vals of
        Nothing -> case lefts vals of
          [] -> Left EmptyInput
          (leftover : _) -> Left $ CouldNotRecognize leftover
        Just progs -> pure progs

compile :: Program -> Either CompilerError ShExpr
compile = return . normalizeShAnds <=< renderProgram <=< compilePhrases
  where
    normalizeShAnds (ShAnd (ShAnd x y) z) = x <> y <> z
    normalizeShAnds expr = expr
    renderProgram :: FuncTable -> Either CompilerError ShExpr
    renderProgram ftab =
      mappend <$> renderDefinitions <*> renderExpressions
      where
        renderDefinitions =
          fmap mconcat
            . sequenceA
            . elems
            . mapWithKey renderDef
            $ delete "" ftab
        renderExpressions =
          maybe (pure mempty) (renderDef "") (ftab !? "")
    renderDef :: String -> FuncType -> Either CompilerError ShExpr
    renderDef "" (Function argc shExpr) =
      pure shExpr
    renderDef funcName (Function _ shExpr) =
      pure $ ShFuncDef funcName shExpr
    renderDef funcName (FuncPlaceholder _) = Left $ FuncNotDef funcName
    renderDef funcName (CaseFunc argc paramName cases) =
      pure $
        ShFuncDef funcName $
          ShCase (ShGetVar paramName) $
            Data.List.NonEmpty.toList cases
    renderDef str (Parameter name) =
      error $
        "FIXME: for function " <> str
          <> " a parameter type ("
          <> name
          <> ") not expected"

-- ~~FoqaRuntime~~? ~~FoqaInnerAST~~? ~~FoqaAST~~? FoqaProgram? FoqaProgState?
type FuncTable = Map String FuncType

data FuncType
  = Function Int ShExpr
  | CaseFunc Int String (NonEmpty (ShArg, ShExpr))
  | Parameter String
  | FuncPlaceholder Int
  deriving (Show)

data CompilerError
  = FuncNotDef String
  | TooLittleArguments String
  | TooManyArguments String (NonEmpty Expression)
  deriving (Eq, Show)

compilePhrases :: Foldable f => f Phrase -> Either CompilerError FuncTable
compilePhrases = foldl compilePhrase (Right mempty)
  where
    compilePhrase ::
      Either CompilerError FuncTable ->
      Phrase ->
      Either CompilerError FuncTable
    compilePhrase (Left err) _ = Left err
    compilePhrase (Right ftab) (Exp e) =
      case compileExprInBody ftab e of
        Left err -> Left err
        Right (shExpr', ftab') -> case ftab !? "" of
          Nothing ->
            return $ singleton "" (Function 0 shExpr') <> ftab' <> ftab
          Just (Function 0 shExpr) ->
            return $
              singleton "" (Function 0 $ shExpr <> shExpr') <> ftab' <> ftab
          Just x ->
            error $
              "FIXME: FuncTable: \"\" function should not be of value "
                <> show x
    compilePhrase (Right ftab) (Def d) =
      second (`mappend` ftab) $ compileDef ftab d

-- NOTE: FuncTable may slowly evolve into bigger and bigger intermediate AST
-- some day it may be the main REPL engine

-- TODO: check for correct amount of arguments
combineFuncs :: FuncType -> FuncType -> FuncType
combineFuncs f@(Parameter _) g =
  error $ "TODO: parameter not expected here: " <> show (f, g)
combineFuncs f g@(Parameter _) =
  error $ "TODO: parameter not expected here: " <> show (f, g)
combineFuncs f@(FuncPlaceholder n1) (FuncPlaceholder n2) = f
combineFuncs (FuncPlaceholder _) f = f
combineFuncs f (FuncPlaceholder _) = f
combineFuncs f@(Function _ _) g@(Function _ _) =
  error $ "TODO: Function collision: " <> show (f, g)
combineFuncs f@(CaseFunc {}) g@(CaseFunc {}) =
  error $ "TODO: CaseFunc combination not implemented yet: " <> show (f, g)
combineFuncs f@(Function _ _) g@(CaseFunc {}) =
  error $ "TODO: function already sealed: " <> show (f, g)
combineFuncs f@(CaseFunc argc parName cases) g@(Function _ altBody) =
  case Data.List.NonEmpty.partition isFallbackCase cases of
    ([], _) -> CaseFunc argc parName $ cases <> pure (ShLit "*", altBody)
    _ ->
      error $
        "TODO: function already sealed with fallback case: " <> show (f, g)
  where
    isFallbackCase (ShLit "*", _) = True
    isFallbackCase _ = False

-- error $ "TODO: function sealing not implemented yet: " <> show (f, g)
-- FIXME: CaseFunc may have been already sealed:
-- check `cases` if (ShLit "*", _) already present

compileDef ::
  FuncTable -> Definition -> Either CompilerError FuncTable
compileDef ftab (FuncDef funcName params expr) = do
  let ftabInBody = recurTab <> paramTab <> ftab
  (body, ftabFromBody) <- compileExprInBody ftabInBody expr
  let ftabAfterBody = ftabFromBody <> ftab
  -- TODO: refactor: Parameter is just a variant of Function
  -- NOTE: Parameter -> InlineFunction?
  let currFunc = buildCaseFunc body $ Prelude.zip params [1 ..]
  let newFunc = case ftab !? funcName of
        Nothing -> currFunc
        Just prevFunc -> prevFunc `combineFuncs` currFunc
  return $ insert funcName newFunc ftabAfterBody
  where
    buildCaseFunc :: ShExpr -> [(Param, Int)] -> FuncType
    buildCaseFunc body ((Id _, _) : ps) = buildCaseFunc body ps
    buildCaseFunc body ((Lit (Str str), idx) : ps) =
      CaseFunc (length params) (show idx) $ pure (ShLit str, body)
    buildCaseFunc body ((Lit (Num num), idx) : ps) =
      CaseFunc (length params) (show idx) $ pure (ShLit $ show num, body)
    buildCaseFunc body [] = Function (length params) body
    recurTab = singleton funcName (FuncPlaceholder (length params))
    paramTab =
      Data.Map.fromList
        . (asGetVar <=< Prelude.zip (show <$> [1 ..]))
        $ params
    asGetVar (idx, Id name) = pure (name, Parameter idx)
    asGetVar (idx, Lit value) = mempty
compileDef ftab (OperDef operName p1 p2 expr) =
  compileDef ftab (FuncDef operName [p1, p2] expr)

compileManyArgs ::
  [Expression] -> FuncTable -> Either CompilerError ([ShArg], FuncTable)
compileManyArgs args ftab =
  fmap (foldMap $ first pure)
    . sequence
    $ compileExprInArg ftab
      <$> args

compileExprInBody ::
  FuncTable -> Expression -> Either CompilerError (ShExpr, FuncTable)
compileExprInBody ftab (FuncCall "eval" params) =
  first (ShRaw . mconcat) <$> compileManyArgs params ftab
compileExprInBody _ (LitExpr (Num numLit)) =
  pure (ShRun $ ShLit "printf" :| [ShLit "%d\\n", ShLit $ show numLit], mempty)
compileExprInBody _ (LitExpr (Str strLit)) =
  pure (ShRun $ ShLit "printf" :| [ShLit "%s\\n", ShLit strLit], mempty)
compileExprInBody ftab (OperCall operName param1 param2) =
  compileExprInBody ftab $ FuncCall operName $ pure param1 <> pure param2
compileExprInBody ftab (FuncCall funcName args) =
  case ftab !? funcName of
    Just (Parameter argName) ->
      case args of
        [] ->
          return
            ( ShRun $ ShLit "printf" :| [ShLit "%s\\n", ShGetVar argName],
              mempty
            )
        (a : as) -> Left (TooManyArguments funcName $ a :| as)
    Just (FuncPlaceholder argc) -> compileWithArgc argc
    Just (CaseFunc argc _ _) -> compileWithArgc argc
    Just (Function paramAmt _) -> compileWithArgc paramAmt
    Nothing -> do
      (shArgs, ftabFromArgs) <- compileManyArgs args ftab
      let ftabWithPlaceholder =
            singleton funcName $ FuncPlaceholder $ length args
      return
        ( ShRun $ ShLit funcName :| shArgs,
          ftabWithPlaceholder <> ftabFromArgs
        )
  where
    compileWithArgc argc = do
      (shArgs, ftabFromArgs) <- compileManyArgs args ftab
      case length args `compare` argc of
        EQ -> return (ShRun $ ShLit funcName :| shArgs, ftabFromArgs)
        LT -> Left $ TooLittleArguments funcName
        GT -> Left $ TooManyArguments funcName (Data.List.NonEmpty.fromList args)

compileExprInArg ::
  FuncTable -> Expression -> Either CompilerError (ShArg, FuncTable)
compileExprInArg _ (LitExpr (Num n)) =
  pure (ShLit $ show n, mempty)
compileExprInArg _ (LitExpr (Str s)) =
  pure (ShLit s, mempty)
compileExprInArg ftab (OperCall operName expr1 expr2) =
  compileExprInArg ftab $ FuncCall operName [expr1, expr2]
compileExprInArg ftab expr@(FuncCall funcName args) =
  case ftab !? funcName of
    Just (Parameter argName) -> return (ShGetVar argName, mempty)
    _ -> first ShSubsh <$> compileExprInBody ftab expr
